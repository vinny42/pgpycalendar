This is a proof-of-concept for a set of PostgreSQL stored functions that enable adding iCalendar items with recurrence, exceptions and aggregates.

- Add (recurring) item to the calendar
- Insert an exception on existing calendar items.
- Create an aggregate of items of a particular status.


